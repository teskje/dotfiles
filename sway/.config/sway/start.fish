#! /usr/bin/fish

set -gx (/usr/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh \
         | string split "=")

set -gx BEMENU_BACKEND wayland
set -gx CLUTTER_BACKEND wayland
set -gx MOZ_ENABLE_WAYLAND 1
#set -gx QT_QPA_PLATFORM wayland-egl
set -gx SDL_VIDEODRIVER wayland

set -gx XKB_DEFAULT_OPTIONS compose:rctrl

sway
