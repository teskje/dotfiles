" colors
syntax on
colorscheme nord

" indentation
set tabstop=4      " tabs are 8 chars          
set softtabstop=4  " convert <TAB> to 4 spaces
set shiftwidth=4
set expandtab

set number                  " line numbers
set cursorline              " highlight current line
filetype plugin indent on   " filetype-specific plugins and indentation
set wildmenu                " visual autocomplete for command menu
set lazyredraw              " redraw only when we need to
set showmatch               " highlight matching parens

" searching
set incsearch
set hlsearch
" turn off highlighting on \<space>
nnoremap <leader><space> :nohlsearch<CR>

" move naturally through wrapped lines
nnoremap <Down> gj
nnoremap <Up> gk

" don't create backup or undo files
set nobackup
set noundofile
