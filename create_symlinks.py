#! /usr/bin/env python3

from pathlib import Path


DOTFILES_ROOT = Path(__file__).resolve().parent

CONFIGS = {
    "alacritty": [
        ("alacritty/.config/alacritty", "~/.config/alacritty"),
    ],

    "git": [
        ("git/.gitconfig", "~/.gitconfig"),
    ],

    "sway": [
        ("sway/.config/sway", "~/.config/sway"),
        ("sway/.config/waybar", "~/.config/waybar"),
    ],

    "tmux": [
        ("tmux/.tmux.conf", "~/.tmux.conf"),
        ("tmux/.config/tmux", "~/.config/tmux"),
    ],

    "vim": [
        ("vim/.vimrc", "~/.vimrc"),
        ("vim/.vim", "~/.vim"),
    ],
}


def link_prog(prog):
    resp = input(f"Link configs for {prog}? [Y/n] ")
    if resp not in ("", "y", "Y"):
        return

    for config, name in CONFIGS[prog]:
        link = Path(name).expanduser()
        target = DOTFILES_ROOT / config
        link.symlink_to(target)


for prog in CONFIGS:
    link_prog(prog)
